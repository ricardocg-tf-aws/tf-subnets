#!/bin/bash
#Files for module
touch variables.tf main.tf outputs.tf README.md .gitlab-ci.yml .kitchen.yml .gitignore
#Integration test
mkdir -p test/example/default
touch test/example/default/main.tf 
touch test/example/default/variables.tf
touch test/example/default/outputs.tf 
mkdir -p test/integration/default/controls
touch test/integration/default/controls/test.rb
touch test/integration/default/inspec.yml