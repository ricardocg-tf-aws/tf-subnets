variable "subnets"{
    description = "Number of subnets to be created"
    type = "string"
    default = 1
}

variable "vpc_id"{
    description = "The ID of the vpc"
    type = "string"
}

variable "subnet_cidrs"{
    description = "CIDR FOR SUBNETS"
    type = "list"
    default = ["10.0.3.0/26"]
}

variable "type" {
    description = "Private/Public"
    default     = "Public"
}