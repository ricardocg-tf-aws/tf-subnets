output "subnets" {
    value = {
        "id"            = aws_subnet.main.*.id
        "cidr_block"    = aws_subnet.main.*.cidr_block
    }

    description = "Map of the id and cidr for every subnet created"
}