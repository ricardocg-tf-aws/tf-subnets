provider "aws" {
  region = "us-east-1"
}

resource "aws_subnet" "main" {
  count                     = var.subnets
  vpc_id                    = var.vpc_id
  cidr_block                = var.subnet_cidrs[count.index]
  map_public_ip_on_launch   = true
  
  tags = {
    Name = "terraform-.${var.type}-subnet.${count.index + 1}"
  }

}