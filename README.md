# AWS Subnets MODULE 

- Deploys Private/Public Subnets in AWS for use in VPC. 

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VPC | The ID of the VPC | String | - |:yes:|
| Subnets | Number of subnets to be created  | String | 1 |:yes:|
| subnet_cidrs | CIDR for each subnet | List |  |:yes:|
| Type | Private/Public subnet | String | Public |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| SUBNETS | Map of the ID and CIDR for every subnet created |

# Usage

```js
module "subnet"{
    source          = "../../../"
    subnets         = "2"
    vpc_id          = module.vpc.vpc_id
    type            = "Private"
    subnet_cidrs    = ["10.0.3.0/26", "10.0.4.0/26"]
}
```