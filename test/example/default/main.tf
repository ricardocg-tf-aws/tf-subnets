
#Deploy VPC 
module "vpc"{
    source                                  = "git::https://gitlab.com/ricardocg-tf-aws/tf-vpc.git?ref=master"
    vpc_cidr                                = "10.0.0.0/16"
    env                                     = "prod"
}

module "subnet"{
    source          = "../../../"
    subnets         = "2"
    vpc_id          = module.vpc.vpc_id
    type            = "Private"
    subnet_cidrs    = ["10.0.3.0/26", "10.0.4.0/26"]
}