output "subnets" {
    value       = module.subnet.subnets 
    description = "Map of the id and cidr for every subnet created"
}